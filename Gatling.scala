package com.blazemeter.demo

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class GatlingDemo extends Simulation {

  val httpProtocol = http
    .baseURL("http://10.10.10.172")    
    .acceptHeader( """text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8""")
    .acceptEncodingHeader( """gzip, deflate""")
    .acceptLanguageHeader( """en-US,en;q=0.5""")
    .connection( """keep-alive""")
    .userAgentHeader( """Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0""")

  val uri1 = """http://10.10.10.172"""

  val scn = scenario("GatlingDemo").repeat(100000) {
    exec(http("request_0")
      .get( """/""")      
      .check(status.is(200)))
  }

  setUp(scn.inject(atOnceUsers(20))).protocols(httpProtocol)


}

